# Features
Adds support for Jilia (OAuth2, client_credentials) authentication in [httpie](https://github.com/jakubroztocil/httpie).  It will store your access_token and append it to all requests for a session.  It will also delete and refresh tokens that have expired.

# Installing/Updating
    sudo pip install git+https://www.bitbucket.org/centralite/httpie-jilia-auth.git#egg=httpie-jilia-auth --upgrade

# Usage
## Create Session
    http --session <name> --auth-type=jilia -a 'client_id:client_secret' <url>
## Use session
    http --session <name> <url>

# Troubleshooting
The oauth tokens are stored at `~/.jilia`.  This location can be overridden with the environment variable `JILIA_TOKEN_STORAGE`

The token request is made to `http://centralite-test.apigee.net/jilia/v1/oauth/accesstoken` or the environment varialbe `JILIA_AUTH_URL`

When in doubt delete the `~/.jilia` directory to remove all tokens.