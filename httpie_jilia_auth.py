"""
Jilia Auth plugin for HTTPie.
"""
import os
from httpie.plugins import AuthPlugin
from requests.auth import AuthBase
import requests
import json
import time
from pprint import pprint
from urlparse import urlparse, urljoin

__version__ = '0.0.2'
__author__ = 'Kenny York'
__licence__ = 'MIT'

class JiliaAuth(AuthBase):
  def __init__(self,key,secret):
    self.key = key
    self.secret = secret

  def parse_site(self, url):
    # need to convert 'http://<site>/<version>/<whatever>'
    # to <site>_<version>
    url = urlparse(url)
    version = url.path.strip('/').split('/')[0]
    self.site = '_'.join([url.netloc,version])

  def load_token(self):
    # use ~/.jilia or JILIA_TOKEN_STORAGE
    base_path = os.path.expanduser(os.getenv('JILIA_TOKEN_STORAGE',os.path.join('~','.jilia')))
    # add the site url to the path
    token_dir = os.path.join(base_path, self.site)

    # create the directory structure if it doesn't exist
    if not os.path.exists(token_dir):
      os.makedirs(token_dir)

    #token file
    self.path = os.path.join(token_dir, "{}:{}".format(self.key, self.secret))

    # if the token file doesn't exist, initialize it
    if not os.path.exists(self.path):
      with open(self.path,'w+') as f:
        f.write('{}')

    with open(self.path,'r') as f:
      self.token_data = json.load(f)

  def save_token(self):
    with open(self.path,'w+') as f:
      f.write(json.dumps(self.token_data, indent=2,separators=(',',': ')))
  
  def request_token(self):
    auth_url = 'https://' + '/'.join([self.site.replace('_','/'),'oauth','accesstoken'])
    #print auth_url
    body = {'grant_type':'client_credentials'}
    
    #print 'Requesting token from: ' + auth_url
    res = requests.post(auth_url, auth=(self.key, self.secret), data=body)
    
    # throw an exception if the request failed
    res.raise_for_status()
    
    json = res.json()
    # return the token and the time it will expire
    return json['access_token'], (time.time() + float(json['expires_in']))

  def __call__(self, r):
    self.parse_site(r.url)
    self.load_token()

    token = self.token_data.get('token')
    valid_until = self.token_data.get('valid_until')

    if (token == None or valid_until == None) or (valid_until != None and time.time() >= valid_until):
      #token not valid, request it
      token, valid_until = self.request_token()
      self.token_data['token'] = token
      self.token_data['valid_until'] = valid_until
      self.save_token()

    r.headers['Authorization'] = 'Bearer ' + token
    return r

class JiliaAuthPlugin(AuthPlugin):

  name = 'Jilia Auth'
  auth_type = 'jilia'
  description = ''

  def get_auth(self, username, password):
    return JiliaAuth(username, password)

if __name__ == '__main__':
  plugin = JiliaAuthPlugin()
  auth = plugin.get_auth('foo','bar')
  res = auth(requests.Request('GET', 'https://api.jilia.io/v1').prepare())
  pprint(res.headers)