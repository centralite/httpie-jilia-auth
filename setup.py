from setuptools import setup
try:
    import multiprocessing
except ImportError:
    pass


setup(
    name='httpie-jilia-auth',
    description='Jilia Auth plugin for HTTPie.',
    version='0.0.1',
    author='Kenny York',
    author_email='kennyyork@centralite.com',
    license='MIT',
    url='https://bitbucket.org/centralite/httpie-jilia-auth',
    download_url='https://bitbucket.org/centralite/httpie-jilia-auth',
    py_modules=['httpie_jilia_auth'],
    zip_safe=False,
    entry_points={
        'httpie.plugins.auth.v1': [
            'httpie_jilia_auth = httpie_jilia_auth:JiliaAuthPlugin'
        ]
    },
    install_requires=[
        'httpie>=0.7.0',
    ],
)